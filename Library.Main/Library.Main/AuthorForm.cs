﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Library.Services;
using Library.ViewModels;

namespace Library.Main
{
    public partial class AuthorForm : BaseForm
    {
        private AuthorViewModel authorViewModel;
        private AuthorService _authorService;
        public AuthorForm(AuthorViewModel author = null)
        {
            InitializeComponent();
            _authorService = new AuthorService();

            if (author != null)
            {
				FillForm(author);
            }
        }

        private void btnSaveAuthor_Click(object sender, EventArgs e)
        {
            authorViewModel = new AuthorViewModel
            {
                Id = string.IsNullOrEmpty(txtAuthorId.Text) ? (int?)null : int.Parse(txtAuthorId.Text),
                FirstName = txtAuthorFirstname.Text,
                LastName = txtAuthorLastname.Text
            };
            this.Close();
        }
    }
}
