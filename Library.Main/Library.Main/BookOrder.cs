﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Library.Services;
using Library.ViewModels;

namespace Library.Main
{
    public partial class BookOrder : Form
    {
        private BookOrderService _bookOrderService;
        private BookOrderViewModel _bookOrderViewModel;
        public BookOrder()
        {
            _bookOrderService = new BookOrderService();
            _bookOrderViewModel = new BookOrderViewModel();
            InitializeComponent();
        }

        private void btnSaveBookReader_Click(object sender, EventArgs e)
        {
            BookReader bookReader = new BookReader(_bookOrderViewModel.BookReader);
            bookReader.FormClosed += BookReader_FormClosed;
            bookReader.ShowDialog();
        }

        private void BookReader_FormClosed(object sender, FormClosedEventArgs e)
        {
            var form = (BookReader)sender;
            lblFirstName.Text = !string.IsNullOrWhiteSpace(form.Firstname) ? form.Firstname : "-";
            lblLastName.Text = !string.IsNullOrWhiteSpace(form.Lastname) ? form.Lastname : "-";
            lblEmail.Text = !string.IsNullOrWhiteSpace(form.Email) ? form.Email : "-";
            lblPhone.Text = !string.IsNullOrWhiteSpace(form.Phone) ? form.Phone : "-";
            txtBookReaderId.Text = form.Id.ToString();

            _bookOrderViewModel.BookReader = new BookReaderViewModel
            {
                Id = form.Id,
                Email = form.Email,
                FirstName = form.Firstname,
                LastName = form.Lastname,
                Phone = form.Phone
            };
        }
    }
}
