﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Library.Services;
using Library.ViewModels;

namespace Library.Main
{
    public partial class BookList : Form
    {
        private AuthorService _autrorService;
        private List<BookViewModel> _books;
        private BookSearchViewModel _searchViewModel;
        private int? _authorId = null;
        public BookList(int? authorId = null)
        {
            _authorId = authorId;
            _autrorService = new AuthorService();
            _books = new List<BookViewModel>();
            _searchViewModel = new BookSearchViewModel();
            InitializeComponent();
            ConfigForm();
            ConfigureBookListDataGridView();
            FillGrid(authorId);
        }

        private void ConfigForm()
        {
            List<int> asd = new List<int>();
            for (int i = 1547; i <= DateTime.Now.Year; i++)
            {
                asd.Add(i);
            }

            cmbxReleaseDate.DataSource = new BindingSource(asd.OrderByDescending(x => x), null);
            cmbxReleaseDate.SelectedIndex = -1;
        }

        private void FillGrid(int? authorId = null)
        {
            _books = _autrorService.GetBooks(authorId: authorId);
            booksGridView.Rows.Clear();
            foreach (var book in _books)
            {
                var index = booksGridView.Rows.Add();
                booksGridView.Rows[index].Cells["BookId"].Value = book.BookId;
                booksGridView.Rows[index].Cells["Name"].Value = book.Name;
                booksGridView.Rows[index].Cells["ReleaseDate"].Value = book.ReleaseDate;
                booksGridView.Rows[index].Cells["Authores"].Value = DisplayAuthores(book.BookAuthores);
                booksGridView.Rows[index].Cells["BookObject"].Value = book;
            }
        }

        private void FillGrid(BookSearchViewModel bookSearchViewModel)
        {
            _books = _autrorService.GetBooks(bookSearchViewModel);
            booksGridView.Rows.Clear();
            foreach (var book in _books)
            {
                var index = booksGridView.Rows.Add();
                booksGridView.Rows[index].Cells["BookId"].Value = book.BookId;
                booksGridView.Rows[index].Cells["Name"].Value = book.Name;
                booksGridView.Rows[index].Cells["ReleaseDate"].Value = book.ReleaseDate;
                booksGridView.Rows[index].Cells["Authores"].Value = DisplayAuthores(book.BookAuthores);
                booksGridView.Rows[index].Cells["BookObject"].Value = book;
            }
        }

        private string DisplayAuthores(List<AuthorViewModel> authores)
        {
            return string.Join(", ", authores.Select(x => x.Fullname));
        }

        private void MockData()
        {
            _books.Add(new BookViewModel
            {
                BookId = 1,
                Name = "Test test test test",
                //ReleaseDate = new DateTime(1998, 12, 12),
            }); 
        }

        private void ConfigureBookListDataGridView()
        {
            DataGridViewColumn BookId = new DataGridViewColumn
            {
                Name = "BookId",
                Visible = false,
                CellTemplate = new DataGridViewTextBoxCell()
            };
            booksGridView.Columns.Add(BookId);

            DataGridViewColumn Name = new DataGridViewColumn
            {
                Name = "Name",
                Width = 200,
                HeaderText = "Name",
                Resizable = DataGridViewTriState.False,
                CellTemplate = new DataGridViewTextBoxCell(),
            };
            booksGridView.Columns.Add(Name);

            DataGridViewColumn ReleaseDate = new DataGridViewColumn
            {
                Name = "ReleaseDate",
                Width = 150,
                HeaderText = "ReleaseDate",
                Resizable = DataGridViewTriState.False,
                CellTemplate = new DataGridViewTextBoxCell(),
            };
            booksGridView.Columns.Add(ReleaseDate);

            DataGridViewColumn Authores = new DataGridViewColumn
            {
                Name = "Authores",
                HeaderText = "Authores",
                CellTemplate = new DataGridViewTextBoxCell(),
                Width = 150
            };
            booksGridView.Columns.Add(Authores);

            DataGridViewColumn bookObject = new DataGridViewColumn
            {
                Name = "BookObject",
                Visible = false,
                CellTemplate = new DataGridViewTextBoxCell()
            };
            booksGridView.Columns.Add(bookObject);

            DataGridViewButtonColumn editButton = new DataGridViewButtonColumn
            {
                Name = "EditBook",
                CellTemplate = new DataGridViewButtonCell(),
                Width = 70,
                HeaderText = "",
                Text = "Edit",
                UseColumnTextForButtonValue = true
            };
            booksGridView.Columns.Add(editButton);

            DataGridViewButtonColumn deleteButton = new DataGridViewButtonColumn
            {
                Name = "Delete",
                CellTemplate = new DataGridViewButtonCell(),
                Width = 70,
                HeaderText = "",
                Text = "Delete",
                UseColumnTextForButtonValue = true
            };
            booksGridView.Columns.Add(deleteButton);
        }

        private void booksGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 5)
            {
                var book = (BookViewModel)((DataGridView)sender).Rows[e.RowIndex].Cells["BookObject"].Value;
                BookSave bookSave = new BookSave(book);
                bookSave.FormClosed += BookSave_FormClosed;
                bookSave.ShowDialog();
            }

            if (e.ColumnIndex == 6)
            {
                DialogResult dialogResult = MessageBox.Show("Delete", "Are you want to delete this book?", MessageBoxButtons.YesNo);

                if (dialogResult == DialogResult.Yes)
                {
                    var bookId = ((BookViewModel)((DataGridView)sender).Rows[e.RowIndex].Cells["BookObject"].Value).BookId;
                    _autrorService.DeleteBook(bookId.Value);
                    FillGrid(_authorId);
                }
            }
        }

        private void BookSave_FormClosed(object sender, FormClosedEventArgs e)
        {
            FillGrid(_authorId);
        }

        private void btnNewBook_Click(object sender, EventArgs e)
        {
            BookSave bookSave = new BookSave();
            bookSave.FormClosed += BookSave_FormClosed;
            bookSave.ShowDialog();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            _searchViewModel.BookAuthor = txtBookAuthor.Text;
            _searchViewModel.BookName = txtBookName.Text;
            _searchViewModel.ReleaseDate = cmbxReleaseDate.SelectedItem != null ? int.Parse(cmbxReleaseDate.SelectedItem.ToString()) : (int?)null;
            FillGrid(_searchViewModel);
        }
    }
}
