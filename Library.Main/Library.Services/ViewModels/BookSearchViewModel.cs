﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.ViewModels
{
    public class BookSearchViewModel
    {
        public string BookName { get; set; }

        public string BookAuthor { get; set; }

        public int? ReleaseDate { get; set; }

        public BookSearchViewModel()
        {

        }
    }
}
